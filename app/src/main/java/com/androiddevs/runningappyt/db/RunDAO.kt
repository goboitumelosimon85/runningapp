package com.androiddevs.runningappyt.db

import androidx.lifecycle.LiveData
import androidx.room.*


@Dao
interface RunDAO {

    //onconflict means when i try to insert the new run and the old exist
    // it must replace it
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertRun(run: Run)

    //when you want to delete a run, added to coroutine
    @Delete
    suspend fun deleteRun(run: Run)


    //sort the list by date(timestamp i Run class), newly added on top
    @Query("SELECT * FROM running_table ORDER BY timestamp DESC")
    fun getallRunsSortedByDate(): LiveData<List<Run>>

    //sort the list by timeInMillis, newly added on top
    @Query("SELECT * FROM running_table ORDER BY timeInMillis DESC")
    fun getallRunsSortedByTimeInMillis(): LiveData<List<Run>>

    //sort the list by calories burned, newly added on top
    @Query("SELECT * FROM running_table ORDER BY caloriesBurned DESC")
    fun getallRunsSortedByCaloriesBurned(): LiveData<List<Run>>

    //sort the list by avgSpeedTime, newly added on top
    @Query("SELECT * FROM running_table ORDER BY avgSpeedTime DESC")
    fun getallRunsSortedByAvgSpeedTime(): LiveData<List<Run>>

    //sort the list by distanceInMeters, newly added on top
    @Query("SELECT * FROM running_table ORDER BY distanceInMeters DESC")
    fun getallRunsSortedByDistance(): LiveData<List<Run>>


    //following functions for Statestics
    @Query("SELECT SUM(timeInMillis) FROM running_table")
    fun getTotalTimeInMillis(): LiveData<Long>

    @Query("SELECT SUM(caloriesBurned) FROM running_table")
    fun getTotalCaloriesBurned(): LiveData<Int>

    @Query("SELECT SUM(distanceInMeters) FROM running_table")
    fun getTotalDistance(): LiveData<Int>

    @Query("SELECT AVG(avgSpeedTime) FROM running_table")
    fun getTotalAvgSpeed(): LiveData<Float>


}