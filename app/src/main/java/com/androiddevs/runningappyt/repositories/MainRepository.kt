package com.androiddevs.runningappyt.repositories

import com.androiddevs.runningappyt.db.Run
import com.androiddevs.runningappyt.db.RunDAO
import javax.inject.Inject

class MainRepository @Inject constructor(
    val  runDAO: RunDAO
){
    suspend fun insertRun(run: Run) = runDAO.insertRun(run)
    suspend fun deleteRun(run: Run) = runDAO.deleteRun(run)

    fun getallRunsSortedByDate() = runDAO.getallRunsSortedByDate()
    fun getallRunsSortedByTimeInMillis() = runDAO.getallRunsSortedByTimeInMillis()
    fun getallRunsSortedByCaloriesBurned() = runDAO.getallRunsSortedByCaloriesBurned()
    fun getallRunsSortedByAvgSpeedTime() = runDAO.getallRunsSortedByAvgSpeedTime()
    fun getallRunsSortedByDistance() =runDAO.getallRunsSortedByDistance()

    fun getTotalTimeInMillis() = runDAO.getTotalTimeInMillis()
    fun getTotalCaloriesBurned() = runDAO.getTotalCaloriesBurned()
    fun getTotalDistance() = runDAO.getTotalDistance()
    fun getTotalAvgSpeed()= runDAO.getTotalAvgSpeed()

}